package com.emd.mclient.user;

import com.emd.mclient.user.dto.UserDto;
import com.emd.mclient.user.service.UserService;

import com.emd.mclient.user.web.exceptions.InvalidArgumentException;
import com.emd.mclient.user.web.exceptions.InvalidUserException;
import com.emd.mclient.user.web.exceptions.UserNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
;
import java.time.*;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
public class UserSeviceTest {

	@Autowired
	private UserService service;


	@Test
	public void shouldCreateNewUserValue() {

		UserDto user1 = UserDto.builder()
				.login("login")
				.password("password")
				.email("email@email.com")
				.lastName("last name")
				.firstName("first name")
				.birthDate(LocalDate.of(2000, Month.DECEMBER, 25))
				.address("20 Avenue de la republique, 93800, epinay-sur-seine")
				.phoneNumber("0123456789")
				.country("France")
				.build();

		UserDto createdUser = service.saveUser(user1);
		System.out.println("Test 1 "+createdUser);
		assertNotNull(createdUser.getId());
		assertEquals(user1.getLogin(), createdUser.getLogin());
		assertTrue(new BCryptPasswordEncoder().matches(
				user1.getPassword(), createdUser.getPassword()
		));
		assertEquals(user1.getLastName(), createdUser.getLastName());
		assertEquals(user1.getFirstName(), createdUser.getFirstName());
		assertEquals(user1.getEmail(), createdUser.getEmail());
		assertEquals(user1.getAddress(), createdUser.getAddress());
		assertEquals(user1.getPhoneNumber(), createdUser.getPhoneNumber());
		assertEquals(user1.getBirthDate(), createdUser.getBirthDate());
		assertEquals(user1.getCountry(), createdUser.getCountry());
	}

	@Test
	public void shouldCreateNewUserWithAddressDefaultValue_whenAdressIsNull() {

		UserDto expected = UserDto.builder()
				.login("login")
				.password("password")
				.email("email@email.com")
				.lastName("last name")
				.firstName("first name")
				.birthDate(LocalDate.of(2000, Month.DECEMBER, 25))
				.phoneNumber("0123456789")
				.address(null)
				.country("France")
				.build();

		assertEquals("14 chemin du marcreux, 93300, Aubervilliers", service.saveUser(expected).getAddress());

	}

	@Test
	public void shouldCreateNewUserWithAddressDefaultValue_whenAdressIsEmpty() {

		UserDto oneUser = UserDto.builder()
				.login("login")
				.password("password")
				.email("email@email.com")
				.lastName("last name")
				.firstName("first name")
				.birthDate(LocalDate.of(2000, Month.DECEMBER, 25))
				.phoneNumber("0123456789")
				.address("")
				.country("France")
				.build();

		UserDto createdUser = service.saveUser(oneUser);
		System.out.println("Test 3 "+createdUser);
		assertEquals("14 chemin du marcreux, 93300, Aubervilliers", createdUser.getAddress());

	}

	@Test
	public void shouldThrowInvalidOperationException() {
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			service.userById(-2L);
		});
		Assertions.assertThrows(InvalidArgumentException.class, () -> {
			service.userById(null);
		});
	}

	@Test
	public void shouldThrowUserNotFoundException() {
		Assertions.assertThrows(UserNotFoundException.class, () -> {
			service.userById(9030393939292992938L);
		});
		Assertions.assertThrows(UserNotFoundException.class, () -> {
			service.userById(0L);
		});
	}

	@Test
	public void shouldThrowInvalidUserException_andShowErrorMessage_whenARequiredFieldIsNull() {

		UserDto dtoUser = UserDto.builder()
				.login(null)
				.password("password")
				.email("email@email.com")
				.lastName("lastname")
				.firstName("first name")
				.birthDate(LocalDate.of(2000, Month.DECEMBER, 25))
				.phoneNumber("0123456789")
				.country("France")
				.build();

		assertEquals("Login is required", assertThrows(InvalidUserException.class, () -> service.saveUser(dtoUser)).fieldsErrorsMessage.get(0));

	}





}
