package com.emd.mclient.user.aspectManagement.pointcuts;

import org.aspectj.lang.annotation.Pointcut;

public class UserControllerPointcuts {

    @Pointcut("execution( public * com.emd.mclient.user.web.controller.UserController.*(..))")
    public void allMethodsOfController(){}
}
