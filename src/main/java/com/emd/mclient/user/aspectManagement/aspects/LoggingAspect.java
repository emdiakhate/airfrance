package com.emd.mclient.user.aspectManagement.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import java.time.Duration;
import java.time.Instant;


@Aspect
@Component
public class LoggingAspect {
    private Instant start;
    private Long duration;

    @Before("com.emd.mclient.user.aspectManagement.pointcuts.UserControllerPointcuts.allMethodsOfController()")
    public void startLog(final JoinPoint joinPoint){
        start = Instant.now();
        System.out.println("INFO : Start calling "+joinPoint.getSignature().getName()+" méthod from "
        +joinPoint.getSignature().getDeclaringTypeName()+" class");
    }

    @After("com.emd.mclient.user.aspectManagement.pointcuts.UserControllerPointcuts.allMethodsOfController()")
    public void endLog(final JoinPoint joinPoint){
        duration = Duration.between(start, Instant.now()).toMillis();
        System.out.println("INFO : End calling "+joinPoint.getSignature().getName()+" méthod from "
                +joinPoint.getSignature().getDeclaringTypeName()+" class");
        System.out.println("INFO : Processing time duration is : "+duration+" milliseconds");
    }
}
