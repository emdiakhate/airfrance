package com.emd.mclient.user.aspectManagement.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("com.emd.mclient.user.web.controller")
public class AspectConfig {
}
