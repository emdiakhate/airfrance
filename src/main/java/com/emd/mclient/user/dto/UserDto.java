package com.emd.mclient.user.dto;


import com.emd.mclient.user.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import javax.validation.constraints.*;


import java.time.LocalDate;
import java.time.Period;

@Getter @Setter @Builder @NoArgsConstructor @AllArgsConstructor @ToString
public class UserDto {

    private Long id;

    @NotBlank(message = "Login is required")
    private String login;

    @NotBlank(message = "Password is required")
    private String password;

    @NotBlank(message = "Firstname is required")
    private String firstName;

    @NotBlank(message = "Lastname is required")
    private String lastName;

    /* Date format : "yyyy/mm/dd" */
    @NotNull(message="Bithdate is required")
    private LocalDate birthDate;

    @Email(message = "Email format not valid")
    @NotBlank(message = "Email is required")
    private String email;

    private String address;

    @NotBlank(message = "Phone number is required")
    @Pattern(regexp="(^$|[0-9]{10})", message = "Phone number must be 10 digits")
    private String phoneNumber;

    @NotBlank()
    private String country;



    public static UserDto entityToDto(User user) {
        if (user == null) {
            return null;
        }

        return UserDto.builder()
                .id(user.getId())
                .login(user.getLogin())
                .password(user.getPassword())
                .email(user.getEmailAdress())
                .lastName(user.getLastName())
                .firstName(user.getFirstName())
                .birthDate(user.getBirthDate())
                .address(user.getAddress())
                .phoneNumber(user.getPhoneNumber())
                .country(user.getCountry())
                .build();
    }

    public static User dtoToEntity(UserDto userDto) {

        if (userDto == null) {
            return null;
        }

        return User.builder()
                .id(userDto.getId())
                .login(userDto.getLogin())
                .password(new BCryptPasswordEncoder().encode(userDto.getPassword()))
                .emailAdress(userDto.getEmail())
                .lastName(userDto.getLastName())
                .firstName(userDto.getFirstName())
                .birthDate(userDto.getBirthDate())
                .address(userDto.getAddress())
                .phoneNumber(userDto.getPhoneNumber())
                .country(userDto.getCountry())
                .build();
    }

    @JsonIgnore
    @AssertTrue(message="Country must be France")
    public boolean isFranceCountry(){
        if(!country.equalsIgnoreCase("france")) {
            return false;
        }
        return true;
    }

    @JsonIgnore
    @AssertTrue(message="Age must be over 18")
    public boolean isBirthDateOverEighteen(){
        int age = Period.between(birthDate, LocalDate.now()).getYears();
        if (age < 18) {
            return false;
        }
        return true;
    }
}
