package com.emd.mclient.user.web.controller;

import com.emd.mclient.user.dto.UserDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static com.emd.mclient.user.endpoints.EndpointsConstant.USER_ENDPOINT;


/**
* Interface for User Controller
* 
* @author Malick
* 
*/
@Api(USER_ENDPOINT)
public interface UserApi {

	@ApiOperation(value = "Register a new user", response = UserDto.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "User created successfully"),
			@ApiResponse(code = 400, message = "Invalid user")
	})
	@PostMapping("/create")
	UserDto create(@RequestBody UserDto user);

	@ApiOperation(value = "Find user", notes = "Search by id", response = UserDto.class)
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "User found"),
			@ApiResponse(code = 404, message = "User not found")
	})
	@GetMapping("/{id}")
	UserDto userById(@PathVariable Long id);

}
