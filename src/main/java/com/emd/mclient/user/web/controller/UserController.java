package com.emd.mclient.user.web.controller;

import com.emd.mclient.user.dto.UserDto;
import com.emd.mclient.user.entity.User;
import com.emd.mclient.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import static com.emd.mclient.user.endpoints.EndpointsConstant.USER_ENDPOINT;

@RestController
@RequestMapping(USER_ENDPOINT)
public class UserController implements UserApi {

    @Autowired
    private UserService userService;


    /**
     * Get user by id
     *
     * @param userDto : user to save
     * @return User created
     */
    @Override
    public UserDto create(UserDto userDto){
       return userService.saveUser(userDto);
    }

    /**
     * Get user by id
     *
     * @param id: identifiant of user to find
     * @return User having this id
     */
    @Override
    public UserDto userById(@PathVariable("id") Long id){
        return userService.userById(id);
    }
}
