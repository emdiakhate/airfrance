package com.emd.mclient.user.web.exceptions;


import lombok.extern.slf4j.Slf4j;
import org.hibernate.type.StringRepresentableType;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.*;

@Slf4j
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidUserException extends RuntimeException {
    public List<String> fieldsErrorsMessage;

    public InvalidUserException(List<String> errors) {
      this.fieldsErrorsMessage =  errors;
      errors.stream().forEach(log::error);
    }

}
