package com.emd.mclient.user.validator;

import com.emd.mclient.user.dto.UserDto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.*;

@Slf4j
@Component
public class UserValidator {
    public List<String> messageErrorList = new ArrayList<>();


    public Boolean isValidEntity(UserDto user) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<UserDto>> constraintViolations = validator.validate(user);

        if (constraintViolations.size() > 0 ) {
            log.info("Unable to validate data ");
            for (ConstraintViolation<UserDto> contraintes : constraintViolations) {
                   messageErrorList.add(contraintes.getMessage());
            }
            return false;
        }
        return true;
    }
}
