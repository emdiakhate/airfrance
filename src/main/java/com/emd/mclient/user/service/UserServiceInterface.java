package com.emd.mclient.user.service;

import com.emd.mclient.user.dto.UserDto;


public interface UserServiceInterface {

    UserDto saveUser(UserDto user);
    UserDto userById(Long userId);
}
