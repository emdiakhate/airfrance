package com.emd.mclient.user.service;

import com.emd.mclient.user.dto.UserDto;
import com.emd.mclient.user.entity.User;
import com.emd.mclient.user.validator.UserValidator;
import com.emd.mclient.user.web.exceptions.InvalidArgumentException;
import com.emd.mclient.user.web.exceptions.InvalidUserException;
import com.emd.mclient.user.web.exceptions.UserNotFoundException;
import com.emd.mclient.user.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


@Slf4j
@Service
public class UserService implements UserServiceInterface {

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserValidator userValidator;

    @Override
    public UserDto saveUser(UserDto user) {
        if(haveNullValue(user))
            user = registerWithDefaultValue(user);
        if(!userValidator.isValidEntity(user))
            throw new InvalidUserException(userValidator.messageErrorList);

        user.setId(sequenceGeneratorService.generateSequence(User.SEQUENCE_NAME));
        return UserDto.entityToDto(userRepository.save(UserDto.dtoToEntity(user)));
    }

    @Override
    public UserDto userById(Long id) {
        if (id == null || id < 0 )
            throw new InvalidArgumentException("Argument not valid");
        return UserDto.entityToDto(userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User with id "+id+" not found")));
    }

    private UserDto registerWithDefaultValue(UserDto user) {
        if (!StringUtils.hasLength(user.getAddress())) {
            user.setAddress("14 chemin du marcreux, 93300, Aubervilliers");
        }
        return user;
    }

    private boolean haveNullValue(UserDto user){
        if(user.getAddress() == null || user.getAddress().isEmpty() )
            return true;
        return false;
    }

}
